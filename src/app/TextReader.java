package app;

/* 
 * This class contains functions that count all the words, show frequency of different word,
 * total number of different words, and shows all different words for the given text file: 
 * @author Lyhov Anderson
*/
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TextReader {
	
	// Total words of the text
	private int totalWords = 0;

	// Store all the words
	private Map<String, Integer> frequency = new HashMap<>();

	// Store all different words
	private Set<String> wordOfText = new HashSet<>();

	/**
	 * This method does all the counting words
	 * 
	 * @param: fileName: input file name to be read from
	 */
	public void countWords(String fileName) throws IOException {

		// passing file path for reading
		FileReader file = new FileReader(fileName);
		BufferedReader reader = new BufferedReader(file);

		// Reading lines from the text
		String line = reader.readLine();

		while (line != null) {
			if (!line.trim().equals("")) {
				// Testing if it actually read lines
				// System.out.println("Processing line: " + line);

				// Split all the words
				String[] words = line.split(" ");

				// Counting total words
				totalWords += words.length;

				// Eliminate white space
				for (String word : words) {
					if (word == null || word.trim().equals("")) {
						continue;
					}
					// Set all the words to lower case, and read only alphabet
					// from a to z
					String cleanedUpWord = word.toLowerCase().replaceAll("[^a-z]", "");

					// Add all the different words to HashSet
					if (cleanedUpWord.length() != 0) {
						wordOfText.add(cleanedUpWord);

						if (getFrequency().containsKey(cleanedUpWord)) {
							// Add cleaned up word to HashMap, and increment
							// each word it is unique
							getFrequency().put(cleanedUpWord, getFrequency().get(cleanedUpWord) + 1);
						} else {
							getFrequency().put(cleanedUpWord, 1);
						}
					}
				}
			}
			// Continue reading line
			line = reader.readLine();
		}
		// Close BufferReader
		reader.close();

		/*
		 * Test the function, and have it printed in the console
		 * 
		 * System.out.print("The count for each word: \n" + this.getFrequency()
		 * + "\n"); System.out.println("Total words: " + totalWords + "\n");
		 * System.out.println("Total number of different words: " +
		 * wordOfText.size() + ", " + "and they are: \n" + wordOfText + "\n");
		 */
	}
    
	/**
	 * This method returns frequency
	 * 
	 * @return: frequency: word frequency
	 */
	public Map<String, Integer> getFrequency() {
		return frequency;
	}
	
	// This method returns all the results
	public String str() {
		return ("The count for each word: \n" + this.frequencyString() + "\n") + ("Total words: " + totalWords + "\n")
				+ ("Total number of different words: " + wordOfText.size() + ", and they are: \n" + wordOfText + "\n");
	}

	public String frequencyString() {
		String answer = "";
		for (Entry<String, Integer> e : frequency.entrySet()) {
			answer += e.getKey() + " = " + e.getValue() + "\n";
		}
		return answer;
	}
}
	
	