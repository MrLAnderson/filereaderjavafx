package app;
/* 
 * This is the main class for our application
 * It integrates fxml file, javaFX libraries, and java classes.
 * @author: Lyhov Anderson
 */

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;

public class GUI extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			// Grab the scene layout from Layout.fxml
			Parent root = FXMLLoader.load(getClass().getResource("/app/Layout.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setTitle("Text Reader");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// Launch the app
	public static void main(String[] args) {
		launch(args);
	}
}
