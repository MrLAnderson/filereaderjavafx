package app;

/*
 * This class is the main controller for the application
 * It handles all the buttons actions.
 * It also has a few return methods.
 * @author: Lyhov Anderson
 */
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class MainController {

	@FXML
	private Button btnOpen;

	@FXML
	private Button btnRead;

	@FXML
	private Button btnExit;

	@FXML
	private TextArea printArea = new TextArea();

	// Global variable
	File selectedFile;

	// Create a new object, so that we can access methods in TextReader class
	TextReader calculate = new TextReader();
	
	/**
	 * Button action for Open File button
	 * 
	 * @param: event
	 */
	public void ButtonOpenAction(ActionEvent event) {
		FileChooser fc = new FileChooser();
		fc.setInitialDirectory(new File("C:\\Users\\Lee's\\Documents\\Augsburg16\\Spring17\\compilers-i\\Test Files"));
		// fc.setInitialDirectory(new File ("C:\\Users"));
		fc.setTitle("Open resource file");
		// Set file extension
		fc.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.txt"),
				new ExtensionFilter("All Files", "*.*"));
		selectedFile = fc.showOpenDialog(null);
		if (selectedFile != null) {
			// Print selected file path to textArea
			printArea.setText(selectedFile.getAbsolutePath());
			try {
				Files.lines(selectedFile.toPath()).forEach(System.out::println);
			} catch (IOException ex) {
				Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			// Print error message to textArea and console if no file selected
			System.out.println("File is not valid");
			printArea.setText("No file is selected");
		}
	}
	

	/**
	 * 
	 * Button action for Read File button
	 * 
	 * @param: event
	 */
	public void ButtonReadAction(ActionEvent event) throws IOException {
		// Unable user to edit text
		printArea.setEditable(false);
		// Call countWords function
		calculate.countWords(this.getFileName());
		// Print all the result to textArea
		printArea.setText(calculate.str());
	}
	
	/**
	 * This method returns file path
	 * 
	 * @return: filepath
	 */
	public String getFileName() {
		return selectedFile.getAbsolutePath();
	}

	/**
	 * Exit window
	 */
	public void ButtonCloseAction() {
		Platform.exit();
	}
}
