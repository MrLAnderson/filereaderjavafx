Word Counter

This is a Java application that reads text from a text document. The main functions of this application include: counts the total number of words, counts the number of different words (ignoring case), and counts the frequency of each different word.

The text reader also ignores all punctuations and special characters. Lastly, it is integrated with JavaFX to have a user interface that can run the application. The output will be displayed within the application.
